from django.test import TestCase
from .models import Makanan, Restoran
from django.urls import reverse, resolve
# Create your tests here.

class IndexTest(TestCase):
    def test_status_code_200(self):
        response = self.client.get(reverse("ppw:index"))
        self.assertEquals(response.status_code, 200)
    def test_html_index(self):
        response = self.client.get(reverse("ppw:index"))
        self.assertTemplateUsed(response,'index.html')