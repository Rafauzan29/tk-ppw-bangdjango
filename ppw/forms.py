from django import forms
from .models import Makanan

class Pembelian(forms.Form):
    beli = forms.IntegerField(label="Masukkan Jumlah", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Masukkan Jumlah',
        'required': False,
    }))