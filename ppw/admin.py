from django.contrib import admin
from .models import Restoran, Makanan

admin.site.register(Restoran)
admin.site.register(Makanan)
