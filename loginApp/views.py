from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib import messages
from .models import Profile
from .forms import LoginForm, RegisterForm
 
User = get_user_model()


def login_page(request):
    form = LoginForm(request.POST or None)
    context = {
        'form': form
    }
    print(request.user.is_authenticated)
    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            print("error.......")

    return render(request, "loginApp/login.html", context=context)

def register_page(request):
    form = RegisterForm(request.POST or None)
    context = {
        'form': form,
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password_first")
        User.objects.create_user(username, email, password)
        messages.success(request, f'Account {username} has been created. Please Log In!')

        
    return render(request, "loginApp/register.html", context=context)

def logout_page(request):
    logout(request)
    return render(request, 'loginApp/logout.html')