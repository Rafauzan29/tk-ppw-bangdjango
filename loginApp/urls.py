from django.contrib import admin
from django.urls import path, include
from .views import login_page, register_page, logout_page

app_name = 'login'

urlpatterns = [
    path('', login_page, name='login'),
    path('register/', register_page, name='register'),
    path('logout/', logout_page, name='logout') 
] 