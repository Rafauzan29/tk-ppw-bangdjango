from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .models import Profile
from .forms import LoginForm, RegisterForm
from .views import login_page, register_page, logout_page
from django.apps import apps
from .apps import LoginappConfig



class LoginAppApps(TestCase):
    def test_apps(self):
        self.assertEqual(LoginappConfig.name, 'loginApp') 
        self.assertEqual(apps.get_app_config('loginApp').name, 'loginApp')


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            "username": "Feenzy",
            "password": "123",
        })

        form_regist = RegisterForm(data={
            "username": "Feenzy",
            "email": "feenzy@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })
        self.assertTrue(form_regist.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_register = RegisterForm(data={})
        self.assertFalse(form_register.is_valid())

    def test_form_regist_is_exist(self):
        form_regist = RegisterForm(data={
            "username": "Feenzy",
            "email": "feenzy@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })
        form_regist = RegisterForm(data={
            "username": "Feenzy",
            "email": "feenzy@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })
        self.assertTrue(form_regist.is_valid())



class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_login_ada(self):
        response = Client().get('/login')
        self.assertEquals(response.status_code, 301)
    
    def test_url_register_ada(self):
        response = Client().get('/login/register')
        self.assertEquals(response.status_code, 301)
    
    def test_url_logout_ada(self):
        response = Client().get('/login/logout')
        self.assertEquals(response.status_code, 301)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_page = reverse("loginApp:login")
        self.register_page = reverse("loginApp:register")
        self.logout_page = reverse("loginApp:logout")
        self.user_new = User.objects.create_user("Feenzy","Feenzy@gmail.com", password='123')
        self.user_new.save()
        self.profile = Profile.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_page, {
            'username': 'Feenzy', 
            'password':'123'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'loginApp/login.html')

    def test_GET_register(self):
        response = self.client.get(self.register_page, {
            'username': 'Feenzy', 
            'email': 'feenzy@gmail.com', 
            'password_first':'123', 
            'password_again':'123'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'loginApp/register.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'loginApp/logout.html')

    def test_register_login_post(self):
        #register
        response = self.client.post(self.register_page, data = {
            "username" : "Feenzy",
            "email" : "Feenzy@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "Feenzy",
            "email" : "Feenzy@gmail.com",
            "password_first" : "123",
            "password_again" : "12",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "FeenzyAgain",
            "email" : "Feenzy123@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })

        response = self.client.post(self.login_page,data ={
            "username" : "Feenzy",
            "password" : "123"
        })
        self.assertEquals(response.status_code,302)

    def test_not_register_yet(self):
        response = self.client.post(self.login_page,data ={
            "username" : "Feenzypro",
            "password" : "123"
        })
        self.assertEquals(response.status_code,200)

        
class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("Feenzy", password="123")
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "Feenzy",
            password = "123"
        )

    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(),1)

    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user,self.new_user)

    def test_to_string(self):
        self.assertIn("Feenzy",str(self.new_user.profile))

        


        


