from django.db import models
from ppw.models import Makanan

# Create your models here.
pmb1 = "Go-pay"
pmb2 = "Ovo"
pmb3 = "Debit"
PMB_CHOICES = (
    (pmb1,"Go-bay"),
    (pmb2,"Owo"),
    (pmb3,"Debit"),
)

pgm1 = "JNO"
pgm2 = "TUKI"
pgm3 = "Si Lambat"
PGM_CHOICES = (
    (pgm1,"JNO"),
    (pgm2,"TUKI"),
    (pgm3,"Si Lambat"),
)

class konfirmasi(models.Model):
    alamat = models.CharField('Alamat Pengiriman',max_length=1000,blank=False)
    pembayaran = models.CharField('Metode Pembayaran',choices= PMB_CHOICES, default=pmb3,blank=False,max_length=50)
    pengiriman = models.CharField('Metode Pengiriman',choices= PGM_CHOICES, default=pgm3,blank=False,max_length=50)
    noHP = models.CharField('Nomor Telefon',max_length=12,blank=False)
    catatan = models.CharField("Catatan",max_length=500,blank=True)
    def __str__(self):
        return f'{self.keranjang}'

class Beli(models.Model):
    makanan = models.ForeignKey(Makanan, on_delete=models.CASCADE)
    jumlah = models.IntegerField(default=0)
    totalharga = models.IntegerField(default=0)
    def __str__(self):
        return self.makanan