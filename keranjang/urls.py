from django.urls import path
from . import views

app_name = 'keranjang'

urlpatterns = [
    path('', views.keranjang, name='keranjang'),
    path('checkout/', views.checkout, name='checkout'),
    path('bayar/', views.bayar, name='bayar'),
    path('detail/', views.detail, name='detail')
]
