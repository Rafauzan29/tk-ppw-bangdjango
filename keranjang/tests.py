from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import konfirmasi
from .forms import KonfirmasiForm

# Create your tests here.
class unit_test_page1 (TestCase):
    def test_url_200(self):
        response = self.client.get(reverse("keranjang:keranjang"))
        self.assertEquals(response.status_code, 200)
    
    def test_template_page1(self):
        response = self.client.get(reverse("keranjang:keranjang"))
        self.assertTemplateUsed(response,'keranjang_landing.html')


class unit_test_page2 (TestCase):
    def test_url_200(self):
        response = self.client.get(reverse("keranjang:checkout"))
        self.assertEquals(response.status_code, 200)

    def test_template_page2(self):
        response = self.client.get(reverse("keranjang:checkout"))
        self.assertTemplateUsed(response,'checkout.html')

    # def test_model_page2(self):
    #     test_keranjang = konfirmasi.objects.create(
    #         alamat = "jakarta",
    #         pembayaran = "Debit",
    #         pengiriman = "JNO",
    #         noHP = "081294785155",
    #         catatan = "proses dengan cepat"
    #     )
    #     n_checkout = test_keranjang.objects.count()
    #     self.assertEqual(n_checkout,1)

    def test_form_is_valid(self):
        form_checkout = KonfirmasiForm(data={
            "alamat" : "jakarta",
            "pembayaran" : "Debit",
            "pengiriman" : "JNO",
            "noHP" : "081294785155",
            "catatan" : "proses dengan cepat"
        })
        self.assertTrue(form_checkout.is_valid())

    # def test_POST_form(self):
    #     response = self.client.post(self.checkout, data = {
    #         "alamat" : "jakarta",
    #         "pembayaran" : "Debit",
    #         "pengiriman" : "JNO",
    #         "noHP" : "081294785155",
    #         "catatan" : "proses dengan cepat"
    #     })
    #     self.assertTrue(form_checkout.is_valid())
    #     self.cleaned_data(["alamat"]="jakarta")
    #     self.cleaned_data(["pembayaran"]="Debit")
    #     self.cleaned_data(["pengiriman"]="JNO")
    #     self.cleaned_data(["noHP"]="081294785155")
    #     self.cleaned_data(["catatan"]="proses  dengan cepat")

class unit_test_page3 (TestCase):
    def test_url_200(self):
        response = self.client.get(reverse("keranjang:bayar"))
        self.assertEquals(response.status_code, 200)
    
    def test_template_page3(self):
        response = self.client.get(reverse("keranjang:bayar"))
        self.assertTemplateUsed(response,'bayar.html')

class unit_test_page4 (TestCase):
    def test_url_200(self):
        response = self.client.get(reverse("keranjang:detail"))
        self.assertEquals(response.status_code, 200)
    
    def test_template_page4(self):
        response = self.client.get(reverse("keranjang:detail"))
        self.assertTemplateUsed(response,'detail.html')
      