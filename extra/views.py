from django.shortcuts import render, redirect
from .forms import QnAForm, QuestForm
from .models import Question_and_Answer, Quest


def aboutus(request):
	context  = {}
	return render(request, 'aboutus.html', context)

def helpcenter(request):
	result = Question_and_Answer.objects.all()
	form = QuestForm()

	if request.method == "POST":
		form = QuestForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('extra:helpcenter')

	context = {'result' : result, 'form':form}
	return render(request, 'helpcenter.html', context)

def collections(request):
	result = Quest.objects.all()
	form = QnAForm()

	if request.method == "POST":
		form = QnAForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('extra:helpcenter')

	context = {'result' : result,'form':form}
	return render(request, 'collections.html',context)

def delete(request, pk):
	item = Quest.objects.get(id = pk)

	if request.method == "POST":
		item.delete()
		return redirect('extra:collections')


	context = {'item':item}
	return render(request, 'deleting.html', context)