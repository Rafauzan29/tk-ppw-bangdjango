from django.test import TestCase, Client
from .models import *
from .views import *
from django.apps import apps
from .apps import ExtraConfig


# Create your tests here.

class appTest(TestCase):

	def test_extraApps(self):
		self.assertEqual(ExtraConfig.name, 'extra') 
		self.assertEqual(apps.get_app_config('extra').name, 'extra')

class URLTesting(TestCase):

	def test_apakah_url_aboutus_ada(self):
		response = Client().get('/extra/')
		self.assertEquals(response.status_code,200) 

	def test_apakah_url_helpcenter_ada(self):
		response = Client().get('/extra/helpcenter/')
		self.assertEquals(response.status_code,200) 

	def test_apakah_url_collections_ada(self):
		response = Client().get('/extra/collections/')
		self.assertEquals(response.status_code,200) 

	# def test_apakah_url_collections_ada(self):
	# 	response = Client().get('/extra/delete/')
	# 	self.assertEquals(response.status_code,200) 

class TextTesting(TestCase):

	def test_apakah_aboutus_ada_visimisi(self):
		response = Client().get('/extra/')
		html_kembalian = response.content.decode('utf8') 
		self.assertIn("Visi", html_kembalian)
		self.assertIn("Misi", html_kembalian)

	def test_apakah_helpcenter_ada_frequentlyAsk(self):
		response = Client().get('/extra/helpcenter/')
		html_kembalian = response.content.decode('utf8') 
		self.assertIn("Frequently Ask", html_kembalian)

	def test_apakah_collections_ada_questionCollection(self):
		response = Client().get('/extra/collections/')
		html_kembalian = response.content.decode('utf8') 
		self.assertIn("Question Collection", html_kembalian)

class TemplateTesting(TestCase):

	def test_apakah_template_aboutus_sesuai(self):
		response = Client().get('/extra/')
		self.assertTemplateUsed(response, 'aboutus.html')

	def test_apakah_template_helpcenter_sesuai(self):
		response = Client().get('/extra/helpcenter/')
		self.assertTemplateUsed(response, 'helpcenter.html')

	def test_apakah_template_collections_sesuai(self):
		response = Client().get('/extra/collections/')
		self.assertTemplateUsed(response, 'collections.html')

class ModelTesting(TestCase):

	def test_apakah_model_QnA_ada(self):
		Question_and_Answer.objects.create(Question = '1+1 = ??', Answer = 'Jawabannya adalah 2')
		countQnA = Question_and_Answer.objects.all().count()
		self.assertEquals(countQnA, 1)

	def test_apakah_model_Quest_ada(self):
		Quest.objects.create(Question = '2+2 = ??')
		countQuest = Quest.objects.all().count()
		self.assertEquals(countQuest, 1)

class viewsTesting(TestCase):

	def test_apakah_helpcenter_handle_post(self):
		args = {
			'Question' : '1+1 = ??',
			'Answer' : 'Jawabannya adalah 2'
		}
		response = Client().post('/extra/collections/', args) 
		avail_Activity = Question_and_Answer.objects.all().count()
		self.assertEquals(avail_Activity,1)

		self.assertEquals(response.status_code, 302)
		self.assertEquals(response['location'], '/extra/helpcenter/')

		response_Text = Client().get('/extra/helpcenter/')
		html_kembalian = response_Text.content.decode('utf8') 
		self.assertIn('1+1 = ??',html_kembalian)
		self.assertIn('Jawabannya adalah 2', html_kembalian)

	def test_apakah_collections_handle_post(self):
		args = {
			'Question' : '2+2 = ??',
		}
		response = Client().post('/extra/helpcenter/', args) 
		avail_Activity = Quest.objects.all().count()
		self.assertEquals(avail_Activity,1)

		self.assertEquals(response.status_code, 302)
		self.assertEquals(response['location'], '/extra/helpcenter/')

		response_Text = Client().get('/extra/collections/')
		html_kembalian = response_Text.content.decode('utf8') 
		self.assertIn('2+2 = ??',html_kembalian)






