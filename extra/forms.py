from django.forms import ModelForm
from .models import Question_and_Answer, Quest

class QnAForm(ModelForm):
	class Meta:
		model = Question_and_Answer
		fields = '__all__'

class QuestForm(ModelForm):
	class Meta:
		model = Quest
		fields = '__all__'
