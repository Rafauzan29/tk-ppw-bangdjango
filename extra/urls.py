from django.urls import path
from . import views

app_name = 'extra'

urlpatterns = [
    path('', views.aboutus, name='aboutus'),
    path('helpcenter/', views.helpcenter, name='helpcenter'),
    path('collections/', views.collections, name='collections'),
    path('delete/<str:pk>/',  views.delete, name = 'delete'),
]
