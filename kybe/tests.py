from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Review
from ppw.models import Restoran

# Create your tests here.
class TestViews(TestCase):
    def test_index_and_status_code(self):
        response = self.client.get(reverse("kybe:allresto"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "search.html")

    def testSearch(self):
        response = Client().post(reverse("kybe:allresto"), {
            "cari":"ok"
        }, follow=True)
        self.assertContains(response, 'Maaf "ok" tidak ditemukan')

    def testReview_testSearch(self):
        test_resto = Restoran.objects.create(
            nama_restoran = "chatime",
            lokasi = "jakarta",
            kategori = "boba",
            foto = "https://www.centralparkjakarta.com/wp-content/uploads/2017/11/Chatime-photo-1.jpg",
            rating ="0.1"
        )
        url =reverse("kybe:resto",args=[test_resto.id])
        response = Client().post(url, {
            "nama" : "kybe",
            "rating" : "4.5",
            "komentar" : "enak"
        }, follow =True)
        self.assertContains(response, "kybe")
        response2 = Client().post(url, {
            "cari":"cha"
            }, follow =True)
        self.assertContains(response, "chatime")


