from django.apps import AppConfig


class KybeConfig(AppConfig):
    name = 'kybe'
