# Generated by Django 3.1.1 on 2020-11-16 09:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kybe', '0003_review_rating'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='rating',
        ),
    ]
