from django.db import models
from ppw.models import Restoran

# Create your models here.
class Review(models.Model):
    reviewer = models.CharField(max_length=100)
    isi = models.TextField(max_length=360)
    rating = models.DecimalField(max_digits=2, decimal_places=1, default='0.0')
    resto = models.ForeignKey(Restoran, on_delete = models.CASCADE)
    def __str__(self):
        return self.reviewer
