from django.urls import path
from . import views

app_name = 'kybe'

urlpatterns = [
    path('', views.restoran, name='allresto'),
    path('<str:pk>/',views.cariResto, name='cariresto'),
    path('hasil/<str:pk>/',views.resto, name='resto')
]
